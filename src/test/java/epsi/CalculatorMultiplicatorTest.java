package epsi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit test for CalculatorMultiplicator.
 */
public final class CalculatorMultiplicatorTest {
    
    /**
     * Test multiplicating two numbers.
     */
    @Test
    public void shouldMultiplyTwoNumbers()
    {
        assertEquals("2 * 3 should be 6", 6, CalculatorMultiplicator.multiply(2, 3));
    }
}

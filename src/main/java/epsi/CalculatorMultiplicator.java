package epsi;

/**
 * The Calculator class.
 *
 */
public final class CalculatorMultiplicator {
    
    /**
     * Multiply two numbers.
     * @param a the first number
     * @param b the second number
     * @return the result of a * b
     */
    public static int multiply(int a, int b) {
        return a * b;
    }

    private CalculatorMultiplicator() {
        throw new IllegalArgumentException("Utility class - do not instantiate");
    }
}
